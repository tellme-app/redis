# web
from aiohttp import web
import aiohttp_cors
import redis

import json
from pathlib import Path
import re
import io

import requests

from unsplash.api import Api
from unsplash.auth import Auth

from PIL import Image
# from random import choice

here = Path(__file__).resolve().parent

routes = web.RouteTableDef()

r = redis.Redis()

client_id = "8RBofTt4scGW-lK6tmJoeE30nUaeP3Xe3HruaxnrPwU"
client_secret = "btLjS_XWCku16EkPtLe17bosl_as3BRt7X0xAdHUmP0"
redirect_uri = "urn:ietf:wg:oauth:2.0:oob"
code = ""

auth = Auth(client_id, client_secret, redirect_uri, code=code)
api = Api(auth)

def create_style(text):
    forma = []
    start = 0
    # text = 'Whoever took the <em>box</em> took Mary <em>box</em>.'
    ex = 0
    while True:
        # print(start)
        match = re.search("<em>(.*?)</em>", text[start:])#r.json()[0]['box']['examples'][0][0])
        if match:
            # print(match)
            forma.append({"text":text[start:match.span()[0]+start],
                         "style": "regular"})
            start += match.span()[1]
            forma.append({"text":re.findall("<em>(.*?)</em>", match.group())[0],
                         "style":'accindent'})
        else:
            forma.append({"text":text[start:],
                         "style": "regular"})
            break

        if ex >= 100:
            break
        ex +=1

    return forma


@routes.post('/set')
async def set(request):
    data = await request.json()

    en, ru, val, examples = data['en'].replace('_', ' '), data['ru'].replace('_', ' '), data['val'], data['examples']

    t = r.mset({"word_"+en+"_"+ru: json.dumps({"val": val, "examples": examples})})

    return web.json_response({"set": t})

@routes.post('/set_free')
async def set_free(request):
    data = await request.json()

    key, value = data['key'], data['value']

    t = r.mset({key: json.dumps(value)})

    return web.json_response({"set": t})


@routes.post('/set_trainstatus')
async def set_free(request):
    data = await request.json()

    story, unit, value = data['story'], data['unit']

    t = r.mset({"trainstatus_"+story + "_" + unit: value.encode('utf-8')})

    return web.json_response({"set": t})

@routes.post('/get_trainstatus')
async def set_free(request):
    data = await request.json()

    # key, value = data['key'], data['value']
    if "story" not in data:
        return web.json_response({"status": "not trained"})

    if 'unit' not in data:
        allunit = []
        for key in r.scan_iter(match="trainstatus_"+data['story']+"*"):
            allunit.append(r.get(key.decode('utf-8')))

        if None in allunit:
            return web.json_response({"status": "not trained"})
        elif "traning" in allunit:
            return web.json_response({"status": "training"})
        elif "awaiting training" in allunit:
            return web.json_response({"status": "awaiting training"})
        elif "trained" in allunit:
            return web.json_response({"status": "trained"})
        else "trained" in allunit:
            return web.json_response({"status": "not trained"})


    trainstatus = r.get('trainstatus_'+ data['story'] + "_" + data['unit'])
    if trainstatus is None:
        return web.json_response({"status": "not trained"})
    else:
        return web.json_response({"status": trainstatus.decode('utf-8')})

@routes.post('/delete')
async def delete(request):
    data = await request.json()

    key = data['key']

    t = r.delete(key)

    return web.json_response({"set": "ok"})    

@routes.post('/set_audio')
async def set_audio(request):

    reader = await request.multipart()

    field = await reader.next()

    data = await field.json() 

    field = await reader.next()

    data_f = await field.read(decode=True)

    t = r.mset({"audio_"+data['text']: bytes(data_f)})

    return web.json_response({"set": t})
    # f = open('test1.wav', 'wb')
    # f.write(data_f)
    # f.close()

@routes.post('/get_audio')
async def get_audio(request):

    data = await request.json()

    audio = r.get('audio_'+ data['text'])
    if audio is None:
        return web.json_response({"audio":"pizdec"})

    response = web.StreamResponse(headers={
        'Content-Type': 'audio/wav',
        'Accept-ranges': 'bytes'
    })
    await response.prepare(request)

    await response.write(audio)

    return response

@routes.post('/get_allkeys')
async def get(request):
    data = await request.json()
    kyes = []
    for key in r.scan_iter(match=data['prefix']+"*"):
        kyes.append(key.decode('utf-8'))

    return web.json_response(kyes)

@routes.get('/get_imgpng')
async def get_img(request):
    data = request.rel_url.query['keywords']

    img = r.get('imgpng_'+ data)
    if img is not None:
        response = web.StreamResponse(headers={
            'Content-Type': 'image/png'
        })
        await response.prepare(request)

        # while True:
        #     datt = audio.data.read(1024)
        #     if not datt:
        #         break
        #     await response.write(datt)
        await response.write(img)

        return response

    photos = api.search.photos(data)['results']
    url = "https://unsplash.com/photos/{}/download".format(photos[0].id)
    req = requests.get(url, allow_redirects=True)
    # api.photo.get(photos[0].id)
    f = open('t.png', 'wb')
    f.write(req.content)
    f.close()

    im = Image.open("t.png")

    # Provide the target width and height of the image
    (width, height) = (im.width // 2, im.height // 2)
    im_resized = im.resize((width, height))

    if im.width == im.height:
        im_resized = im.resize((500, 500))
    elif im.width > im.height:
        kf = im.height / 500
        (width, height) = (int(im.width // kf), 500)
        print((width, height))
        im_resized = im.resize((width, height))
        crop_kf = int((width - 500) /2)
        (left, upper, right, lower) = (crop_kf, 0, width-crop_kf, 500)
        im_resized = im_resized.crop((left, upper, right, lower))
        im_resized = im_resized.resize((500, 500))
    #     im_resized = im_resized.convert("P", palette=Image.ADAPTIVE)
        im_resized = im_resized.convert('RGB')
    else:
        kf = im.width / 500
        (width, height) = (500, int(im.height // kf))
        im_resized = im.resize((width, height))
        crop_kf = int((height - 500) /2)
        (left, upper, right, lower) = (0, crop_kf, 500, height-crop_kf)
        im_resized = im_resized.crop((left, upper, right, lower))
        im_resized = im_resized.resize((500, 500))
        im_resized = im_resized.convert("P", palette=Image.ADAPTIVE)
        
    
    imgByteArr = io.BytesIO()
    im_resized.save(imgByteArr, format='PNG')
    imgByteArr = imgByteArr.getvalue()
    # im_resized.save('img.png')
    img = imgByteArr #open("img.png" , "rb").read()


    t = r.mset({"imgpng_"+data: bytes(img)})
    r.close()

    response = web.StreamResponse(headers={
            'Content-Type': 'image/png'
    })
    await response.prepare(request)

    # while True:
    #     datt = audio.data.read(1024)
    #     if not datt:
    #         break
    #     await response.write(datt)
    await response.write(img)


@routes.get('/get_img')
async def get_img(request):
    data = request.rel_url.query['keywords']

    img = r.get('imgjpg_'+ data)
    if img is not None:
        response = web.StreamResponse(headers={
            'Content-Type': 'image/jpg'
        })
        await response.prepare(request)

        # while True:
        #     datt = audio.data.read(1024)
        #     if not datt:
        #         break
        #     await response.write(datt)
        await response.write(img)

        return response

    photos = api.search.photos(data)['results']
    url = "https://unsplash.com/photos/{}/download".format(photos[0].id)
    req = requests.get(url, allow_redirects=True)
    # api.photo.get(photos[0].id)
    f = open('t.png', 'wb')
    f.write(req.content)
    f.close()

    im = Image.open("t.png")

    # Provide the target width and height of the image
    (width, height) = (im.width // 2, im.height // 2)
    im_resized = im.resize((width, height))
    size = 1000

    if im.width == im.height:
        im_resized = im.resize((size, size))
        im_resized = im_resized.convert('RGB')
    elif im.width > im.height:
        kf = im.height / size
        (width, height) = (int(im.width // kf), size)
        print((width, height))
        im_resized = im.resize((width, height))
        crop_kf = int((width - size) /2)
        (left, upper, right, lower) = (crop_kf, 0, width-crop_kf, size)
        im_resized = im_resized.crop((left, upper, right, lower))
        im_resized = im_resized.resize((size, size))
    #     im_resized = im_resized.convert("P", palette=Image.ADAPTIVE)
        im_resized = im_resized.convert('RGB')
    else:
        kf = im.width / size
        (width, height) = (size, int(im.height // kf))
        im_resized = im.resize((width, height))
        crop_kf = int((height - size) /2)
        (left, upper, right, lower) = (0, crop_kf, size, height-crop_kf)
        im_resized = im_resized.crop((left, upper, right, lower))
        im_resized = im_resized.resize((size, size))
        im_resized = im_resized.convert('RGB')
        # im_resized = im_resized.convert("P", palette=Image.ADAPTIVE)
        
    
    imgByteArr = io.BytesIO()
    im_resized.save(imgByteArr, format='JPEG')
    imgByteArr = imgByteArr.getvalue()
    # im_resized.save('img.png')
    img = imgByteArr #open("img.png" , "rb").read()


    t = r.mset({"imgjpg_"+data: bytes(img)})
    r.close()

    response = web.StreamResponse(headers={
            'Content-Type': 'image/jpg'
    })
    await response.prepare(request)

    # while True:
    #     datt = audio.data.read(1024)
    #     if not datt:
    #         break
    #     await response.write(datt)
    await response.write(img)

@routes.post('/get')
async def get(request):
    data = await request.json()
    ret = []
    if "ru" in data and "en" in data:

        en, ru, cnt_context = data['en'].replace('_', ' '), data['ru'].replace('_', ' '), data['cnt_context']
        key = "word_"+en+"_"+ru
        exam = r.get(key)
        if exam is not None:
            exams = json.loads(exam)
            exam = []
            stop_ex = 1
            for j in exams['examples']:
                exam.append([create_style(j[0]), create_style(j[1])])
                if stop_ex >= cnt_context:
                    break
            ret = exam
        else:
            ret = []


    elif "en" in data:
        ret = []
        en, cnt, cnt_context = data['en'].replace('_', ' '), data['cnt_words'], data['cnt_context']
        tra = r.get("en_"+en)
        if tra is not None:
            trans = json.loads(tra)
        else:
            trans = {}

        try:
            trans = {k: v for k, v in sorted(trans.items(), key=lambda item: int(item[1]), reverse=True)}
        except:
            pass

        stop = 1
        for i in trans:
            exam = r.get("word_"+en+"_"+i)
            if exam is not None:
                exams = json.loads(exam)
                exam = []
                stop_ex = 1
                for j in exams['examples']:
                    exam.append([create_style(j[0]), create_style(j[1])])
                    if stop_ex >= cnt_context:
                        break
                ret.append({"name":i, "contexts": exam})
            else:
                ret.append({"name":i, "contexts": []})
            if stop >= cnt:
                break
            stop+=1

    elif "ru" in data:
        ret = []
        ru, cnt,cnt_context = data['ru'].replace('_', ' '), data['cnt_words'], data['cnt_context']
        tra = r.get("ru_"+ru)
        if tra is not None:
            trans = json.loads(tra)
        else:
            trans = {}

        try:
            trans = {k: v for k, v in sorted(trans.items(), key=lambda item: int(item[1]), reverse=True)}
        except:
            pass

        stop = 1
        for i in trans:
            exam = r.get("word_"+i+"_"+ru)

            if exam is not None:
                exams = json.loads(exam)
                exam = []
                stop_ex = 1
                for j in exams['examples']:
                    exam.append([create_style(j[1]), create_style(j[0])])
                    if stop_ex >= cnt_context:
                        break

                ret.append({"name":i, "contexts": exam})
            else:
                ret.append({"name":i, "contexts": []})
            if stop >= cnt:
                break
            stop+=1

    return web.json_response(ret)

app = web.Application()
# aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader(str(here)))
app.add_routes(routes)

cors = aiohttp_cors.setup(app, defaults={
    "*": aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers="*",
            allow_headers="*",
        )
})
# app.router.add_post('/', store_mp3_handler)
for route in list(app.router.routes()):
    cors.add(route)
#web.run_app(app)
web.run_app(app,host='0.0.0.0', port = '8003')